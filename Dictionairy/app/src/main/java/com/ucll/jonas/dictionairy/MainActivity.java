package com.ucll.jonas.dictionairy;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String SOAP_ACTION = "http://services.aonaware.com/webservices/Define";
    private static final String METHOD_NAME = "Define";
    private static final String NAMESPACE = "http://services.aonaware.com/";
    private static final String URL = "http://services.aonaware.com/DictService/DictService.asmx";


    private static final String TAG = MainActivity.class.getName();
    private static String word;
    private List<String> definitions;
    EditText text;
    Button searchButton;
    TextView textView;

    private static String defs = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (EditText) findViewById(R.id.text);
        searchButton = (Button) findViewById(R.id.search);
        textView = (TextView) findViewById(R.id.definitions);

        definitions = new ArrayList<String>();
        definitions.add("hey");
        definitions.add("specht");
        definitions.add("gnoom");
        definitions.add("baard");

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text.getText().length() != 0 && text.getText().toString() != "") {
                    word = text.getText().toString();

                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                    //String resultString = "";
                    //for(String s : definitions)
                    //    resultString += s + "  ";
                    //textView.setText("definities van: " + word + " --- " + resultString);
                    // getDefinitions(word);
                } else {
                    textView.setText("Please enter Word");
                }
            }
        });
    }

    private class AsyncCallWS extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            Log.i(TAG, "doInBackground");
            getDefinitions(word);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i(TAG, "onPostExecute");
            //String resultString = "";
            //for(String s : definitions)
            //    resultString += s + "  ";
            textView.setText("definities van: " + word + " --- " + defs);
        }

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
            textView.setText("Searching...");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            Log.i(TAG, "onProgressUpdate");
        }

    }

    private void getDefinitions(String word) {

        // Create request and set its parameters
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("word", word);

        // Soap serialization enveloping
        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        // asmx is dotNet based
        soapEnvelope.dotNet = true;
        soapEnvelope.setOutputSoapObject(request);

        // set the transportation
        HttpTransportSE aht = new HttpTransportSE(URL);

        try {
            aht.call(SOAP_ACTION, soapEnvelope);
            SoapPrimitive result = (SoapPrimitive) soapEnvelope.getResponse();
            defs = result.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }


    }

/*    private void getDefinitions(String _word) {
        SoapObject request = new SoapObject(URL, "Define");
        PropertyInfo wordPI = new PropertyInfo();
        wordPI.setName("Word");
        wordPI.setValue(_word);
        request.addProperty(wordPI);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        try {
            androidHttpTransport.call("Define", envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            defs = response.toString();
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
