package com.ucll.jonas.chronometer;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView timeView;
    Button startStopButton;
    Button resetButton;
    long accumulatedMilliseconds = 0;
    long accumulatedSeconds = 0;
    long accumulatedMinutes = 0;
    Handler handler;
    static Boolean started = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timeView = (TextView) findViewById(R.id.timeView);
        startStopButton = (Button) findViewById(R.id.startStopButton);
        resetButton = (Button) findViewById(R.id.resetButton);

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartStopClicked();
            }
        });
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResetClicked();
            }
        });

        handler = new Handler();
    }

    void increment() {
        accumulatedMilliseconds += 1;
        if(accumulatedMilliseconds == 10) {
            accumulatedMilliseconds = 0;
            accumulatedSeconds++;
        }
        if(accumulatedSeconds == 60) {
            accumulatedSeconds = 0;
            accumulatedMinutes++;
        }
    }

    void reset() {
        accumulatedMilliseconds  = 0;
        accumulatedSeconds = 0;
        accumulatedMinutes = 0;
    }

    private void updateTimeView() {
        timeView.setText(appendZero(accumulatedMinutes) + ":" + appendZero(accumulatedSeconds) + ":" + String.valueOf(accumulatedMilliseconds));
    }

    private String appendZero(Long time) {
        String result = "";
        if(time < 10) result = "0";
        result += String.valueOf(time);
        return result;
    }

    public void onStartStopClicked() {
        updateTimeView();
        if(started == false) startHandler();
        else stopHandler();
    }

    void stopHandler() {
        handler.removeCallbacksAndMessages(null);
        startStopButton.setText(R.string.startTimer);
        started = false;
    }

    void startHandler() {
        startStopButton.setText(R.string.stopTimer);
        scheduleNextTick();
        started = true;
    }

    public void onResetClicked() {
        reset();
        updateTimeView();
    }

/*    @Override
    public void onSaveInstanceState(Bundle bundle) {
        handler.removeCallbacksAndMessages(null);
        bundle.putInt("COUNT", count);
        super.onSaveInstanceState(bundle);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("COUNT");
        updateTimeView();
        if(started) startHandler();
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        accumulatedMilliseconds = preferences.getLong("ms", 0);
        accumulatedSeconds = preferences.getLong("sec", 0);
        accumulatedMinutes = preferences.getLong("min", 0);
        updateTimeView();
        if(started) startHandler();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacksAndMessages(null);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("ms", accumulatedMilliseconds);
        editor.putLong("sec", accumulatedSeconds);
        editor.putLong("min", accumulatedMinutes);
        editor.apply();
        super.onPause();
    }

    private void scheduleNextTick() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onTick();
            }
        }, 100);
    }

    private void onTick() {
        increment();
        updateTimeView();
        scheduleNextTick();
    }

}
